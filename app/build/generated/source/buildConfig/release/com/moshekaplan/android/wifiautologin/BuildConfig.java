/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.moshekaplan.android.wifiautologin;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.vidroid.com.br.apps.ufpr.sem.fio";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 7;
  public static final String VERSION_NAME = "1.0.0-beta1";
}
